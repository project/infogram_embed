<?php

declare(strict_types=1);

namespace Drupal\infogram_embed\Plugin\Block;

use Drupal\Core\Utility\Token;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Annotation\ContextDefinition;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the infogram embed block.
 *
 * @Block(
 *   id = "infogram_embed",
 *   admin_label = @Translation("Infogram Embed"),
 *   category = @Translation("Infogram Embed"),
 *   context_definitions = {
 *     "entity" = @ContextDefinition("entity", label = @Translation("Entity"), required = FALSE)
 *   }
 * )
 */
class InfogramEmbedBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritDoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Token $token,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->token = $token;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('token'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return [
      'title' => NULL,
      'uuid' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    if ($this->moduleHandler->moduleExists('token')) {
      $wrapper_id = 'infogram-embed-block';

      $form['#prefix'] = "<div id='{$wrapper_id}'>";
      $form['#suffix'] = '</div>';

      $form['context_mapping']['entity']['#ajax'] = [
        'event' => 'change',
        'method' => 'replace',
        'wrapper' => $wrapper_id,
        'callback' => [$this, 'ajaxFormCallback'],
      ];
      $parents = $form['#parents'] ?? [];
      $configuration = $this->getConfiguration();

      $context_mapping = $this->getFormStateValue(
        array_merge($parents, ['context_mapping', 'entity']),
        $form_state,
        $configuration['context_mapping']['entity'] ?? NULL
      );

      if (isset($context_mapping)) {
        $entity_type = substr(
          $context_mapping, strrpos($context_mapping, ':') + 1
        );

        $form['tokens'] = [
          '#theme' => 'token_tree_link',
          '#token_types' => [$entity_type],
          '#dialog' => TRUE,
        ];
      }
    }

    return $form;
  }

  /**
   * Ajax form callback.
   *
   * @param array $form
   *   The form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   An array of the form to return via ajax.
   */
  public function ajaxFormCallback(
    array $form,
    FormStateInterface $form_state
  ): array {
    $button = $form_state->getTriggeringElement();

    return NestedArray::getValue(
      $form,
      array_splice($button['#array_parents'], 0, -2)
    );
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm(
    $form,
    FormStateInterface $form_state
  ): array {
    $configuration = $this->getConfiguration();
    $form = parent::blockForm($form, $form_state);

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Input the infogram embed title.'),
      '#default_value' => $configuration['title']
    ];
    $form['uuid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('UUID'),
      '#required' => TRUE,
      '#description' => $this->t('Input the infogram embed UUID.'),
      '#default_value' => $configuration['uuid']
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function blockSubmit(
    $form,
    FormStateInterface $form_state
  ): void {
    $this->setConfiguration($form_state->cleanValues()->getValues());
  }

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $configuration = $this->getTokenizedConfigurations();

    if (!isset($configuration['uuid']) || empty($configuration['uuid'])) {
      return [];
    }

    return [
      '#uuid' => $configuration['uuid'],
      '#title' => $configuration['title'],
      '#theme' => 'infogram_embed',
      '#attached' => [
        'library' => [
          'infogram_embed/embed-loader'
        ]
      ]
    ];
  }

  /**
   * Get tokenized configurations.
   *
   * @return array
   *   An array of tokenized configurations.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getTokenizedConfigurations(): array {
    $configuration = $this->getConfiguration();

    /** @var \Drupal\Core\Entity\EntityInterface $node */
    if ($entity = $this->getContextValue('entity')) {
      $process_keys = ['title', 'uuid'];
      $process_configs = array_intersect_key(
        $configuration, array_flip($process_keys)
      );

      foreach ($process_configs as $name => $value) {
        $configuration[$name] = $this->processToken($value, $entity);
      }
    }

    return $configuration;
  }

  /**
   * Process the token value.
   *
   * @param string $value
   *   The value to process.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that will be used as token data.
   *
   * @return string
   *   The processed token value.
   */
  protected function processToken(
    string $value,
    EntityInterface $entity
  ): string {
    $entity_type_id = $entity->getEntityTypeId();

    return $this->token->replace(
      trim($value),
      [$entity_type_id => $entity],
      ['clear' => TRUE]
    );
  }

  /**
   * Get the form state value.
   *
   * @param $key
   *   The key to the form state value.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param mixed $default_value
   *   The default value.
   *
   * @return mixed
   */
  protected function getFormStateValue(
    $key,
    FormStateInterface $form_state,
    $default_value = NULL
  ) {
    if (!is_array($key)) {
      $key = [$key];
    }

    $inputs = [
      $form_state->getValues(),
      $form_state->getUserInput(),
    ];

    foreach ($inputs as $input) {
      $key_exists = FALSE;
      $value = NestedArray::getValue($input, $key, $key_exists);

      if ($key_exists) {
        return $value;
      }
    }

    return $default_value;
  }

}
