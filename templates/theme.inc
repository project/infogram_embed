<?php

/**
 * @file
 * Define the infogram embed theme implementation.
 */

declare(strict_types=1);

/**
 * Template preprocess for the infogram embed.
 *
 * @param $variables
 *   An array of template variables.
 */
function template_preprocess_infogram_embed(&$variables): void {
  $element = $variables['element'];

  $variables['uuid'] = $element['#uuid'] ?? NULL;
  $variables['title'] = $element['#title'] ?? NULL;
}
