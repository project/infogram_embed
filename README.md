Infogram Embed
===========

The module allows a site-builder to embed [Infogram](https://infogram.com/)
charts using the block framework. It has token integration so that it's possible
to place an Infogram chart once, and have it dynamically switched out.

Installation
------------

* Normal module installation procedure. See
  https://www.drupal.org/documentation/install/modules-themes/modules-8

Initial Setup
------------

1. Navigate, `/admin/structure/block`.
2. Click `Place block` for the region you want to display the Infogram.
3. Select the `Infogram Embed` from the block list.
4. Input the `Infogram UUID` which you can get from the Infogram website under
   the share embed code.

   Optionally, you can input an `Infogram Title` which is passed along to the `data-title` attribute.
